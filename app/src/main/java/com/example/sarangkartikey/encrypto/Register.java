package com.example.sarangkartikey.encrypto;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;

public class Register extends AppCompatActivity {

    EditText first_name, last_name,email,phone,age,gender,address;
    ImageView back_register;
    ImageView next_register;
    TextView register_text;
    Typeface raleway;
    ProgressClass pc;
    String first_name_string,last_name_string, email_string, phone_string, age_string, gender_string,address_string, username_string;
    OkHttpClient client;
    AlertClass ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        Bundle bundle = getIntent().getExtras();

        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        client = new OkHttpClient();

        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        age = (EditText) findViewById(R.id.age);
        gender = (EditText) findViewById(R.id.gender);
        address = (EditText) findViewById(R.id.address);

        register_text = (TextView) findViewById(R.id.register_text);

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        register_text.setTypeface(raleway);

        back_register = (ImageView) findViewById(R.id.back_register);
        next_register = (ImageView) findViewById(R.id.next_register);

        back_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        next_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_name_string = first_name.getText().toString();
                last_name_string = last_name.getText().toString();
                email_string = email.getText().toString();
                phone_string = phone.getText().toString();
                age_string = age.getText().toString();
                gender_string = gender.getText().toString();
                address_string = address.getText().toString();

                if(first_name_string.trim().isEmpty()){
                    first_name.setError("Name is required!");
                    first_name.requestFocus();
                    return;
                }

                if(last_name_string.trim().isEmpty()){
                    last_name.setError("Password is required!");
                    last_name.requestFocus();
                    return;
                }

                if(phone_string.trim().isEmpty()){
                    phone.setError("Password is required!");
                    phone.requestFocus();
                    return;
                }

                if(age_string.trim().isEmpty()){
                    age.setError("Password is required!");
                    age.requestFocus();
                    return;
                }

                register();

            }
        });
    }


    private void register() {
        pc.startProgress("Registering...");
        Log.i("text - ", first_name_string + " " + last_name_string + " " + email_string + " " + phone_string + " " + age_string + " " + gender_string + " " + address_string + " ");
        //Use this to post useful information to the log eg that you have successfully connected to a server. Basically use it to report successes. View all logs in LogCat

        final Map<String, String> param = new HashMap<String, String>();
        param.put("firstname", first_name_string);
        param.put("lastname", last_name_string);
        param.put("email", email_string);
        param.put("phone", phone_string);
        param.put("age", age_string);
        param.put("gender", gender_string);
        param.put("address", address_string);



        //to link java file to php file using json structure for storing data
        new AsyncTask<String, String, String>(){

            String response;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);
                try {
                    JSONObject obj = new JSONObject(s);
                    String status = obj.getString("status");
                    if(status.equals("SUCCESS")){
                        String id = obj.getString("id");
                        MyFunctions.saveDataToPreference(Register.this, id, first_name_string,last_name_string,email_string,phone_string,age_string,gender_string,address_string,"");
                        MyFunctions.setLoginType(Register.this, ConstantsLocal.LOGIN_TYPE_NORMAL);
                        pc.stopProgress();
                        Toast.makeText(Register.this, "Registered Successfully",Toast.LENGTH_SHORT);
                        Intent intent = new Intent(Register.this, Home.class);
                        startActivity(intent);
                    }else {
                        String error = obj.getString("error");
                        if(error.equals("EXISTS")){
                            pc.stopProgress();
                            ac.showOkDialog("Mobile Number Already registered.","");
                        }else{
                            pc.stopProgress();
                            ac.showOkDialog("Some Error Occurred.","");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.USER_REGISTER_URL);
    }
}

