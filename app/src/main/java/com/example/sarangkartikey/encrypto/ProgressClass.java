package com.example.sarangkartikey.encrypto;
//Used for showing dialog box/notification to user

import android.app.ProgressDialog;
import android.content.Context;


public class ProgressClass {
    static Context context;
    public ProgressClass(Context context){
        this.context = context;
        progress=new ProgressDialog(context);
    }

    static ProgressDialog progress;
    public  static void  startProgress(String msg){
        progress.setMessage(msg);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();

    }

    public static void startLinearProgress(){
        progress = new ProgressDialog(context);
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.show();
    }
    public static void stopProgress(){
        progress.cancel();
    }
    public  static boolean isActive(){return progress.isShowing();}
}
