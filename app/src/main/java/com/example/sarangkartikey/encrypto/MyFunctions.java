package com.example.sarangkartikey.encrypto;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.widget.Toast;


public class MyFunctions {
    public  static SharedPreferences sp;
    public static boolean isUserLoggedIn(Context context){
        if(context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getBoolean(ConstantsLocal.IS_USER_LOGIN, false)) return true;
        return false;
    }

    public static void setLoginType(Context context, String type){
        sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(ConstantsLocal.LOGIN_TYPE_NAME, type);
        editor.commit();
    }
    public static String getLoginType(Context context){
        return context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.LOGIN_TYPE_NAME,"");
    }
    public static void clearPreferenceFile(Context context){
        sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }
    public static void saveDataToPreference(Context context, String id, String first_name, String last_name, String email, String phone, String age, String gender, String address, String username) {
        SharedPreferences sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(ConstantsLocal.IS_USER_LOGIN, true);
        editor.putString(ConstantsLocal.USER_ID, id);
        editor.putString(ConstantsLocal.USER_FIRST_NAME, first_name);
        editor.putString(ConstantsLocal.USER_LAST_NAME, last_name);
        editor.putString(ConstantsLocal.USER_EMAIL, email);
        editor.putString(ConstantsLocal.USER_PHONE, phone);
        editor.putString(ConstantsLocal.USER_AGE, age);
        editor.putString(ConstantsLocal.USER_GENDER, gender);
        editor.putString(ConstantsLocal.USER_ADDRESS, address);
        editor.putString(ConstantsLocal.USER_USERNAME, username);
        editor.commit();
    }
    public  static void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public static boolean isInternetAvailable(Context context) {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }

    public static boolean checkService(String services, String service){
        if(services.indexOf(service) != -1){
            return true;
        } else {
            return false;
        }
    }


    public static String getUserName(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_FIRST_NAME,"");
    }
    public static String getUserPhone(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_PHONE,"");
    }



}
