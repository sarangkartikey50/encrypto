package com.example.sarangkartikey.encrypto;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Home extends AppCompatActivity {

    TextView welcome_text, file_a_report, track_report,send_location, logout_text;
    Typeface light, medium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {

        light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        welcome_text = findViewById(R.id.welcome_text);
        file_a_report = findViewById(R.id.file_a_report);
        track_report = findViewById(R.id.track_report);
        send_location = findViewById(R.id.send_location);
        logout_text = findViewById(R.id.logout_text);

        welcome_text.setTypeface(light);
        logout_text.setTypeface(medium);

        String str = Build.BRAND + "\n" + Build.DEVICE + "\n" + Build.FINGERPRINT + "\n" + Build.MANUFACTURER + "\n" + Build.MODEL + "\n" + Build.HOST;


        welcome_text.setText("Welcome, " + MyFunctions.getUserName(Home.this));

        file_a_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, FileReport.class));
            }
        });
        track_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, trackreport.class));
            }
        });
        //send_location.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        startActivity(new Intent(Home.this, sendlocation.class));
        //    }
        //});
        logout_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
            }
        });
    }

    private void logOut() {

        MyFunctions.clearPreferenceFile(this);

        Intent intent = new Intent(Home.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
