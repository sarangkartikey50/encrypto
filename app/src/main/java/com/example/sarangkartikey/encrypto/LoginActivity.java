package com.example.sarangkartikey.encrypto;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class LoginActivity extends AppCompatActivity {

    private Button login;
    private EditText mobile;
    private EditText pass;
    private TextView signup;
    private TextView freeuser;
    private TextView dont;
    public  String server_response;
    private OkHttpClient client;
    private ProgressClass pc;
    private AlertClass ac;
    String name, email, img_url;
    Typeface raleway;

    private static final int GOOGLE_REQ_CODE = 2002;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

    }

    private void init() {
        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        if(!MyFunctions.isInternetAvailable(this)){
            ac.showOkDialog("Please Connect to Internet !!","");
        }

        login = (Button) findViewById(R.id.login);
        mobile = (EditText) findViewById(R.id.user_mobile);
        pass = (EditText)findViewById(R.id.user_password);
        signup = (TextView) findViewById(R.id.signup);
        freeuser = (TextView) findViewById(R.id.freeuser);
        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");

        //MediaPlayer mp = MediaPlayer.create(this, R.raw.soho);

        //FONTS

        signup.setTypeface(raleway);
        mobile.setTypeface(raleway);
        pass.setTypeface(raleway);


        login.setTransformationMethod(null);
        //fb_btn.setTransformationMethod(null);
        //g_btn.setTransformationMethod(null);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MyFunctions.isInternetAvailable(LoginActivity.this)){
                    ac.showOkDialog("Please Connect to Internet...","Login Failed");
                    return;
                }

                pc = new ProgressClass(LoginActivity.this);
                pc.startProgress("Logging in...");
                //Intent intent = new Intent(LoginActivity.this, user_register.class);
                //startActivity(intent);
                String mobile_string = mobile.getText().toString();
                String pass_string = pass.getText().toString();

                Log.i("log : ", mobile_string + " " + pass_string);

                checkLogin(mobile_string, pass_string, ConstantsLocal.LOGIN_URL);

            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            startActivity(new Intent(LoginActivity.this, Register.class));
            }
        });

        freeuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, Home.class));
            }
        });


    }




    private void launchHomeScreen(){
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }



    private void checkLogin(String mobile_string, String pass_string, String url) {

        final Map<String, String> param = new HashMap<String, String>();
        Log.d("repos", mobile_string);
        param.put("username", mobile_string);
        param.put("password", pass_string);
        param.put("device", Build.BRAND + " " + Build.MODEL);
        param.put("fingerprint", Build.FINGERPRINT);

        new AsyncTask<String, String, String>(){

            String response;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(
                            client, params[0],
                            RequestServer.LoginBody(param));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return  response;
            }

            @Override
            protected void onPostExecute(String params) {
                super.onPostExecute(params);
                String response = params;
                if(response== null){
                    if(pc.isActive())
                        pc.stopProgress();
                    ac.showOkDialog("Something went wrong","");
                    return;
                }

                JSONArray jsonArray = null;
                try {
                    Log.d("resposne -- ", response);
                    JSONObject obj = new JSONObject(response);

                    String status = obj.get("status").toString();
                    Log.i("Status ",status);

                    if(status.equals("SUCCESS")){
                        String id = obj.getString("id");
                        String firstname = obj.getString("firstname");
                        String lastname = obj.getString("lastname");
                        String age = obj.getString("age");
                        String gender = obj.getString("gender");
                        String phone = obj.getString("phone");
                        String username = obj.getString("username");
                        String address = obj.getString("address");
                        String email = obj.getString("email");

                        Log.d("decrypt", firstname);

                        MyFunctions.saveDataToPreference(LoginActivity.this, id, firstname,lastname,email,phone, age, gender,address,username);
                        launchHomeScreen();

                    }else{

                        String errorType = obj.getString("error");
                        Log.i("ErrorType ",errorType);
                        if(errorType.equals("PHONE_ERROR")){
                            ac.showOkDialog("Email is invalid","");
                        } else if(errorType.equals("PASSWORD_ERROR")){
                            ac.showOkDialog("Password is wrong...","");
                        } else if(errorType.equals("MYSQLI_ERROR")){
                            ac.showOkDialog("Some Error Occured","");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(pc.isActive())
                    pc.stopProgress();
            }
        }.execute(url);
    }

}
