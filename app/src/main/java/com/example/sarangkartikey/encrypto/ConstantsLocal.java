package com.example.sarangkartikey.encrypto;



public class ConstantsLocal {
    //SHARED PREFERENCE RELATED CONSTANTS
    public static String SHARED_PREFERENCE_NAME = "com.example.sarfraz.ems.SHARED_PREFERENCE_NAME";
    public static String IS_USER_LOGIN = "com.example.sarfraz.ems_IS_USER_LOGIN";
    public static String IS_FIRST_TIME_USER = "com.example.sarfraz.ems.IS_FIRST_TIME_USER";
    public static String LOGIN_TYPE_NAME = "com.example.sarfraz.ems.LOGIN_TYPE_NAME";
    public static String LOGIN_TYPE_NORMAL = "com.example.sarfraz.ems.LOGIN_TYPE_NORMAL";
    public static String LOGIN_TYPE_GOOGLE = "com.example.sarfraz.ems.LOGIN_TYPE_GOOGLE";
    public static String LOGIN_TYPE_FACEBOOK = "com.example.sarfraz.ems.LOGIN_TYPE_FACEBOOK";
    public static String REQUEST_MODE = "com.example.sarfraz.ems.REQUEST_MODE";

    //USER PROFILE
    public static String USER_ID = "com.example.sarfraz.ems.USER_ID";
    public static String USER_FIRST_NAME = "com.example.sarfraz.ems.USER_FIRST_NAME";
    public static String USER_LAST_NAME = "com.example.sarfraz.ems.USER_LAST_NAME";
    public static String USER_EMAIL = "com.example.sarfraz.ems.USER_EMAIL";
    public static String USER_PHONE = "com.example.sarfraz.ems.USER_PHONE";
    public static String USER_AGE = "com.example.sarfraz.ems.USER_AGE";
    public static String USER_GENDER = "com.example.sarfraz.ems.USER_GENDER";
    public static String USER_ADDRESS = "com.example.sarfraz.ems.USER_ADDRESS";
    public static String USER_USERNAME =  "com.example.sarfraz.ems.USER_USERNAME";


    //SERVER LINKS
    public static String HOME_URL = "http://sanddunesviewladakh.com/hello/";
    public static String USER_REGISTER_URL = HOME_URL + "register.php";
    public static String LOGIN_URL = HOME_URL + "login.php";
    public static String REPORT_URL = HOME_URL + "report.php";
    public static String GET_STATUS_URL = HOME_URL + "getReportStatus.php";
}
