package com.example.sarangkartikey.encrypto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class generatedreportid extends AppCompatActivity {

    String report_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generatedreportid);
        init();
    }

    private void init()
    {
        TextView etreportid;
        Button logout2, back2;
        Bundle bundle = getIntent().getExtras();
        report_id = bundle.getString("report_id");

        etreportid = (TextView)findViewById(R.id.report_id);
        etreportid.setText(report_id);
        logout2 = (Button)findViewById(R.id.logout2);
        back2 = (Button)findViewById(R.id.back2);

        logout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(generatedreportid.this, LoginActivity.class));
            }
        });

        back2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(generatedreportid.this, Home.class));
            }
        });

    }
}
