package com.example.sarangkartikey.encrypto;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class trackreport extends AppCompatActivity {

    TextView tvstatus, etstatus;
    EditText reportid;
    Button logout1, back, show_btn;
    RelativeLayout id_layout, status_layout;
    OkHttpClient client;
    ProgressClass pc;
    AlertClass ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trackreport);
        init();
    }

    private void init()
    {
        tvstatus = (TextView)findViewById(R.id.tvstatus);
        reportid = (EditText)findViewById(R.id.reportid);
        etstatus = (TextView)findViewById(R.id.etstatus);
        logout1 = (Button)findViewById(R.id.logout1);
        back = (Button)findViewById(R.id.back);
        show_btn = (Button) findViewById(R.id.show_btn);
        id_layout = (RelativeLayout) findViewById(R.id.id_layout);
        status_layout  = (RelativeLayout) findViewById(R.id.status_layout);

        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        status_layout.setVisibility(View.GONE);


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(trackreport.this, LoginActivity.class));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(trackreport.this, Home.class));
            }
        });

        show_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("show", "true");
                String id = reportid.getText().toString();
                getReportStatus(id, ConstantsLocal.GET_STATUS_URL);
            }
        });

    }

    private void getReportStatus(String id, String url) {

        final Map<String, String> param = new HashMap<String, String>();
        Log.d("repos", id);
        param.put("id", id);



        new AsyncTask<String, String, String>(){

            String response;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(
                            client, params[0],
                            RequestServer.LoginBody(param));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return  response;
            }

            @Override
            protected void onPostExecute(String params) {
                super.onPostExecute(params);
                String response = params;
                if(response== null){
                    if(pc.isActive())
                        pc.stopProgress();
                    ac.showOkDialog("Something went wrong","");
                    return;
                }

                JSONArray jsonArray = null;
                try {
                    if(pc.isActive())
                        pc.stopProgress();
                    Log.d("resposne -- ", response);
                    JSONObject obj = new JSONObject(response);

                    String status = obj.get("status").toString();
                    Log.i("Status ",status);

                    if(status.equals("success")){

                        if(obj.get("report_status").equals("0")){
                            etstatus.setText("NOT APPROVED");
                        } else if(obj.get("report_status").equals("1")){
                            etstatus.setText("APPROVED");
                        }

                        status_layout.setVisibility(View.VISIBLE);
                        id_layout.setVisibility(View.GONE);

                    }else{
                        if(obj.get("error").equals("NO_DATA")){
                            etstatus.setText("No Data Found.");
                            status_layout.setVisibility(View.VISIBLE);
                            id_layout.setVisibility(View.GONE);
                        } else {
                            ac.showOkDialog("There was some error!","");
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(pc.isActive())
                    pc.stopProgress();
            }
        }.execute(url);
    }

}

