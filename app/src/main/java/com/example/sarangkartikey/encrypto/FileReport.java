package com.example.sarangkartikey.encrypto;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class FileReport extends AppCompatActivity {

    RadioGroup radio;
    EditText fir;
    Button submit;
    OkHttpClient client;
    ProgressClass pc;
    AlertClass ac;
    Bundle bundle;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_report);
        init();

    }

    private void init(){
        radio = (RadioGroup)findViewById(R.id.radio);
        radio.clearCheck();
        final String[] type = {""};
        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);
        bundle = new Bundle();
        RadioButton rb1 = (RadioButton) findViewById(R.id.Others);
        rb1.setChecked(true);
        sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        final String email = sp.getString(ConstantsLocal.USER_EMAIL, "");
        final String user_id = sp.getString(ConstantsLocal.USER_ID, "");
        ac.showOkDialog(email,"");
        //Log.i("email", email);
        fir = (EditText)findViewById(R.id.fir);
        submit = (Button)findViewById(R.id.submit);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pc.startProgress("Please wait...");
                String report = fir.getText().toString();
                RadioButton rb = (RadioButton) radio.findViewById(radio.getCheckedRadioButtonId());
                type[0] = rb.getText().toString();
                sendReport(type[0], report, email, ConstantsLocal.REPORT_URL, user_id);
                //Log.d("output", rb.getText().toString()+report);
            }
        });


    }

    private void sendReport(String type, String report, String email, String url, String user_id) {

        final Map<String, String> param = new HashMap<String, String>();
        Log.d("repos", email);
        param.put("type", type);
        param.put("report", report);
        param.put("email", email);
        param.put("user_id", user_id);



        new AsyncTask<String, String, String>(){

            String response;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(
                            client, params[0],
                            RequestServer.LoginBody(param));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return  response;
            }

            @Override
            protected void onPostExecute(String params) {
                super.onPostExecute(params);
                String response = params;
                if(response== null){
                    if(pc.isActive())
                        pc.stopProgress();
                    ac.showOkDialog("Something went wrong","");
                    return;
                }

                JSONArray jsonArray = null;
                try {
                    if(pc.isActive())
                        pc.stopProgress();
                    Log.d("resposne -- ", response);
                    JSONObject obj = new JSONObject(response);

                    String status = obj.get("status").toString();
                    Log.i("Status ",status);

                    if(status.equals("success")){

                        bundle.putString("report_id", obj.getString("report_id"));
                        startActivity(new Intent(FileReport.this, generatedreportid.class).putExtras(bundle));
                    }else{
                        ac.showOkDialog("There was some error!","");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(pc.isActive())
                    pc.stopProgress();
            }
        }.execute(url);
    }
}
